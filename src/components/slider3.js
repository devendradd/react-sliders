import React, { Component } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import './slider2.css';


//this component is using NPM package to get the image slider (NPM package is react-responsive-carousel)
export default class Slider3 extends Component {
    render() {
        return (
            <div>                                
                <Carousel className="containerCarousel">
                    <div>
                        <img alt="" src="https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/aurora.jpg" />
                        <p className="legend">Legend 1</p>
                    </div>
                    <div>
                        <img alt="" src="https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/canyon.jpg" />
                        <p className="legend">Legend 2</p>
                    </div>
                    <div>
                        <img alt="" src="https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/city.jpg" />
                        <p className="legend">Legend 3</p>
                    </div>
                </Carousel>
            </div>
        );
    }
}
