import React, {Component} from 'react';
import $ from 'jquery';
const setTimeoutTime = 3000;

//javascript slide show embedded in react js component
export default class Slider extends Component{
    constructor(props){
        super(props);
        this.state = {images: ["https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/aurora.jpg",
                              "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/canyon.jpg",
                              "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/city.jpg",
                              "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/desert.jpg",
                              "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/mountains.jpg",
                              "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/redsky.jpg",
                              "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/sandy-shores.jpg",
                              "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/tree-of-life.jpg"]
                            }
        console.log("this is props from app: ", this.props.imageArray);
        this.slideShowPromise = this.slideShowPromise.bind(this);
        this.loadImages = this.loadImages.bind(this);
    }

    //promise created
    slideShowPromise(image){
      return new Promise((resolve, reject) => {
          setTimeout(() => {
              //directly attach the image to src property of image tag,
              // we have discovered the image tag src value using document.slide object
              resolve(document.slide.src = image);
          }, setTimeoutTime);
      });
      // return promise;
    }

    //async and await
    async loadImages(){

        const fadeInTIme = 1500;
        const fadeOutTime = 1500;

        document.slide.src = this.state.images[0]; // show first image by default

          for(var n = 1 ; n <= this.state.images.length; n++){

                $('.slide').fadeIn(fadeInTIme);// fade in for half of the time i.e. 2500 miliseconds
                $('.slide').fadeOut(fadeOutTime);// fade out for half of the time i.e. 2500 miliseconds

                let result = await this.slideShowPromise(this.state.images[n]); // wait till the promise gets really resolved

                console.log("result is : ", result);

                if(n === this.state.images.length-1){ //check if the value of n in for loop has become 7 i.e. images.length-1
                        console.log("here");
                        n =  -1; //assigned -1 because when last element gets executed the value of n becomes 1 instead of 0.
                                // we want to start it with 0 because the slider should re-initialize to 0 to start all over again with first image.
                        console.log("value of n is : ", n, this.state.images[n]);
                }
          }
    }



    render(){
        window.onload = this.loadImages;
        console.log("this is props from app: ", this.props.imageArray);
        return(
            <div>
                <div className="sliderDiv">

                    <img className="slide" id="slideImage" name="slide" height="500" width="1000" alt=""/>
                </div>
            </div>
        )
    }
}
