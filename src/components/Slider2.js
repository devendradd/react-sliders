import React, { Component } from 'react';
import { Container, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import IosArrowDropleft from 'react-ionicons/lib/IosArrowDropleft';
import IosArrowDropright from 'react-ionicons/lib/IosArrowDropright';
import './slider2.css';
// import imageCarInfo from './carInfo';

export default class Slider2 extends Component {

  constructor(props){
    super(props);
    console.log('image array is :',this.props.imageArray);
    this.state = {
      imageCount: 0,
      currentImage:undefined,
      modal: false,
      modaltText: '',
      modalTitle:''      
    }

    //bind the methods here as we are not using (arrow function which gets auto binded)
    this.previousImage = this.previousImage.bind(this);
    this.nextImage = this.nextImage.bind(this);
    this.displayImageInfo = this.displayImageInfo.bind(this);
  }


  //load the first image by default when the component mounts, 
  //set the image and count to 0 in state
  componentDidMount(){
    var count = this.state.imageCount;
    var image = this.props.imageArray[count].image;
    this.setState({imageCount: 0, currentImage: image, modalDisplay:''});
  }

  //function to show the information about car after clicking on the image.
  displayImageInfo(e) {
    var title = "";
    var text = "";
    this.props.imageArray.forEach(element => {
      if(element.image === e.target.src){
         title = element.title;
         text = (
          <div>  
            <p>
                {element.text}
            </p>
            <a href={element.wikiLink}> click here for more information about {element.title}  </a>
          </div>  
        );
      }
    });
    
    this.setState({
      modal: !this.state.modal,
      modaltText: text,
      modalTitle: title
    });
  }

  //take array from props 
  //method to load the next image
  //add the counter by +1 for each image
  nextImage(){
    var count= undefined;
    var image = undefined;
    if(this.state.imageCount < 7){
      count = this.state.imageCount;
      image = this.props.imageArray[count+1].image;
      this.setState({imageCount: count+1, currentImage: image});
    }else{
      count = -1;
      image = this.props.imageArray[count+1].image;
      this.setState({imageCount: count+1, currentImage: image});
    }
  }

  //method to load the previous image 
  //subtract the counter by -1 for each image 
  previousImage(){
    var count= undefined;
    var image = undefined;
    if(this.state.imageCount > 0){
      count = this.state.imageCount;
      image = this.props.imageArray[count-1].image;
      this.setState({imageCount: count-1, currentImage: image});
    }else{
      count = 8;
      image = this.props.imageArray[count-1].image;
      this.setState({imageCount: count-1, currentImage: image});
    }
  }


  render() {
    var displayString = '';
    if(this.state.modal === true){
      displayString = (
        <Modal isOpen={this.state.modal}>
          <ModalHeader >{this.state.modalTitle}</ModalHeader>
          <ModalBody>
            {this.state.modaltText}
          </ModalBody>
          <ModalFooter>           
            <Button color="secondary" onClick={this.displayImageInfo}>ok</Button>
          </ModalFooter>
        </Modal>
      );
    }
    return (
      <Container className="containerMargin">       
        <Row>
          <Col className="arrowVerticalCenter"><IosArrowDropleft  onClick={this.previousImage} fontSize="60px" color="#43853d" /></Col>
          <div className="imageDiv" onClick={this.displayImageInfo} ref={node => this.img = node}> 
            <Col><img className="slide" id="slideImage" name="slide" height="500" width="800" alt="" src={this.state.currentImage}/></Col>
          </div>
          <Col className="arrowVerticalCenter"><IosArrowDropright  onClick={this.nextImage} fontSize="60px" color="#43853d" /></Col>
        </Row>
        {displayString}
        <h5 className="text">
            Please click image for more information
        </h5>
      </Container>
    );
  }
}

