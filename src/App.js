import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Slider1 from '../src/components/slideshow';
import Slider2 from './components/Slider2';
import Slider3 from '../src/components/slider3'
import 'bootstrap/dist/css/bootstrap.min.css';

import {
  Container, Row, Col, Navbar, Nav, NavLink, NavItem
} from 'reactstrap';

// function to send the props to the component in the react router as it is not taking the {this.state.images} directly
// Belwo is the Higher order component (HOC): taking component as a parameter and returning the new component
function slider2WithProps(Component, props) {
  return function(matchProps) {
    return <Component {...props} {...matchProps} />
  }
}

// slider1WithProps
function slider1WithProps(Component, props) {
  return function(matchProps) {
    return <Component {...props} {...matchProps} />
  }
}

class App extends Component {

  constructor(props){
    super(props);
    this.state={
      images: [
        {
          index:1,
          image:"https://61571e67bd8f1f6acb1a-642f5177af5aebf9ccc5072ad2e9ac32.ssl.cf1.rackcdn.com/WDDYJ8AA4JA018878/ba06895b3dc99b74a769d4b7a2a8d252.jpg",
          title:"Mercedes-Benz AMG GT",
          text: "The Mercedes-AMG GT (C190 / R190) is a two-door, two-seater sports car produced in coupé and roadster form by Mercedes-AMG. The car was introduced on 9 September 2014 and was officially unveiled to the public in October 2014 at the Paris Motor Show.[4] After the SLS AMG, it is the second sports car developed entirely in-house by Mercedes-AMG. Lewis Hamilton assisted with the development. The Mercedes-AMG GT went on sale in two variants (GT and GT S) in March 2015, while a GT3 racing variant of the car was introduced in 2015. A GT4 racing variant, targeted at semi-professional drivers and based on the GT R variant, was introduced in 2017. All variants are assembled at the Mercedes-Benz plant in Sindelfingen, Germany.",
          wikiLink:"https://en.wikipedia.org/wiki/Mercedes-AMG_GT"
        },
        {
          index:2,
          image: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/2007_Hummer_H3_--_NHTSA.jpg/1200px-2007_Hummer_H3_--_NHTSA.jpg",
          title: "Hummer H3",
          text: "The Hummer H3 is a sport utility vehicle/off-road vehicle from Hummer that was produced from 2005 to 2010 by GM, not AM General like the earlier H1 and H2. It was introduced for the 2006 model year, based on a modified GMT355 that underpinned the Chevrolet Colorado/GMC Canyon compact pickup trucks that were also built at GM's Shreveport Operations in Shreveport, Louisiana and the Port Elizabeth plant in South Africa. The H3 was the smallest Hummer model. It was available either as a traditional midsize SUV or as a midsize pickup known as the H3T. The H3 and the Colorado/Canyon are very different. GM claims they share only 10% of their components, with the chassis modified and reinforced for heavy off-road duties.",
          wikiLink:"https://en.wikipedia.org/wiki/Hummer_H3"
        },
        { 
          index:3,
          image:"https://server86.texasdirectauto.com/5YJXCBE40GF003666/002_0002.jpg",
          title:"Tesla Model X",
          text:"The Tesla Model X is a mid-size all-electric luxury crossover utility vehicle (CUV) made by Tesla, Inc. that uses falcon wing doors for access to the second and third row seats. The prototype was unveiled at Tesla's design studios in Hawthorne on February 9, 2012.[11][12] The Model X has an official EPA rated 237–295 mi (381–475 km) range[7][13] and the combined fuel economy equivalent and energy consumption for the AWD P90D was rated at 89 mpg‑e (39 kW⋅h/100 mi)",
          wikiLink:"https://en.wikipedia.org/wiki/Tesla_Model_X"
        },
        
        { 
          index:4,
          image:"http://3-photos5.motorcar.com/used-2015-lamborghini-huracan--13253-16319411-2-800.jpg",
          title:"Lamborghini-Huracan",
          text:"The Lamborghini Huracán (Spanish for hurricane [uɾaˈkan]) is a sports car manufactured by Italian automotive manufacturer Lamborghini replacing the previous V10 flagship, the Gallardo.[3] The Huracán made its worldwide debut at the 2014 Geneva Auto Show,[4] and was released in the market in the second quarter of 2014. The LP 610-4 designation comes from the fact that this car has 610 metric horsepower and 4 wheel drive, while LP stands for Longitudinale Posteriore, which refers to the longitudinal mid-rear engine position.",
          wikiLink:"https://en.wikipedia.org/wiki/Lamborghini_Hurac%C3%A1n"
        },
        
        { 
          index:5,
          image:"https://www.rrmcsterling.com/assets/stock/presskit/white/640/usc50rrc071a01305.jpg",
          title:"Rolls-Royce-Ghost",
          text:"The Rolls-Royce Ghost is a British full-size luxury car manufactured by Rolls-Royce Motor Cars. The Ghost nameplate, named in honour of the Silver Ghost, a car first produced in 1906, was announced in April 2009 at the Auto Shanghai show. During development, the Ghost was known as the RR04. Designed as a smaller, more measured, more realistic car than the Phantom, aiming for a lower price category for Rolls-Royce models, the retail price is around £170,000. The production model was officially unveiled at the 2009 Frankfurt Motor Show. The Ghost Extended Wheelbase was introduced in 2011.",
          wikiLink:"https://en.wikipedia.org/wiki/Rolls-Royce_Ghost"
        },
        
        { 
          index:6,
          image:"https://carsales.pxcrush.net/carsales/car/dealer/fe0b7b1b24a8be0607a54c4fdd76bb00.jpg?pxc_method=crop&pxc_size=670%2C447",
          title:"Mesarati- Gran Turismo Sport",
          text:"Gran Turismo Sport[a] is a racing video game developed by Polyphony Digital and published by Sony Interactive Entertainment for PlayStation 4. It is the thirteenth game in the Gran Turismo video game series, seventh in the main series and the first game in the series to be released for PlayStation 4. Gran Turismo Sport was announced at the 2015 Paris Games Week, and was released worldwide in October 2017, receiving generally favorable reviews from critics",
          wikiLink:"https://en.wikipedia.org/wiki/Gran_Turismo_Sport"
        },
        
        { 
          index:7,
          image:"https://icdn2.digitaltrends.com/image/2018-bmw-m5-review-21-640x640.jpg",
          title:"BMW M5",
          text:"The BMW M5 is a high performance variant of the 5 Series executive car built by the Motorsport division of BMW. The first incarnation of the M5 was hand-built in 1986 on the 535i chassis with a modified engine from the M1 that made it the fastest production sedan at the time.[1] Versions of the M5 have been built from subsequent generations of the 5 Series platform.",
          wikiLink:"https://en.wikipedia.org/wiki/BMW_M5"
        },
        
        { 
          index:8,
          image:"https://images.fineartamerica.com/images-medium-large-5/3-2007-ferrari-f430-spider-f1-jill-reger.jpg",
          title:"Ferrari-458 spider",
          text:"The Ferrari 458 Italia is a mid-engined sports car produced by the Italian sports car manufacturer Ferrari. The 458 replaced the Ferrari F430, and was first officially unveiled at the 2009 Frankfurt Motor Show.[4][5] It was replaced by the Ferrari 488, which was unveiled at the 2015 Geneva Motor Show.",
          wikiLink:"https://en.wikipedia.org/wiki/Ferrari_458#458_Spider_.282011.E2.80.932015.29"
        }
      ]
    }
  }

  render() {
    return (
      <div>

        <div className="App">

        <Navbar fixed="top" color="light" light expand="xs" className="border-bottom border-gray bg-white " style={{ height: 80 }}>

            <Container>
              <Row className="position-relative w-100 align-items-center">

                <Col className="d-none d-lg-flex justify-content-start">
                  <Nav className="mrx-auto" >

                    <NavItem className="d-flex align-items-center">
                      <NavLink className="font-weight-bold" href="/">
                        <img src={'https://raw.githubusercontent.com/Ashwinvalento/cartoon-avatar/master/lib/images/male/45.png'} alt="avatar" className="img-fluid rounded-circle" style={{ width: 36 }} />
                      </NavLink>
                    </NavItem>

                    <NavItem className="d-flex align-items-center">
                      <NavLink className="font-weight-bold" href="/">Auto Slide</NavLink>
                    </NavItem>

                    <NavItem className="d-flex align-items-center">
                      <NavLink className="font-weight-bold" href="/slider2">Manual Slide</NavLink>
                    </NavItem>

                    <NavItem className="d-flex align-items-center">
                      <NavLink className="font-weight-bold" href="/slider3">Third Party Slider</NavLink>
                    </NavItem>


                  </Nav>
                </Col>
              </Row>
            </Container>

          </Navbar>


            <header className="App-header">
                <BrowserRouter>
                    <div>
                        <Switch>
                            <Route path="/" exact component={
                                slider1WithProps(Slider1, {
                                   imageArray: this.state.images
                                  })
                                }/>
                            <Route path="/slider2" exact component={
                                slider2WithProps(Slider2, {
                                  imageArray: this.state.images
                                  })
                                }/>
                            <Route path="/slider3" exact component={Slider3} />
                        </Switch>
                    </div>
                </BrowserRouter>
            </header>
        </div>
      </div>
    );
  }
}

export default App;
